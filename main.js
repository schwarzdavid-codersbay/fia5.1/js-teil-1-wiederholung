function createTableCell(tableCellContent, tableRow) {
  const td = document.createElement('td')
  td.textContent = tableCellContent
  tableRow.append(td)
  return td
}

const table = document.getElementById('table')
console.dir(table)
if(table) {
  table.style.backgroundColor = "red"
  table.classList.add('my-table')
}
console.log(document.querySelector('ol li'))

function createPersonRow() {
  const tr = document.createElement('tr')
  createTableCell('Hans', tr)
  createTableCell('Müller', tr)
  document.getElementById('table').append(tr)
}

const myButton = document.getElementById('button')
myButton.addEventListener('click', function () {
  console.log('button click')
  createPersonRow()
})

myButton.addEventListener('mouseover', function () {
  myButton.textContent = 'Hover'
})

myButton.addEventListener('mouseout', function () {
  myButton.textContent = 'Drück mich'
})

document.getElementById('form').addEventListener('submit', function (event) {
  event.preventDefault() // verhindert seitenreload durch form
  console.log('submit')
})
